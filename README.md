# CodingAppTest
## Coding Test - Android


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

CodingAppTest es una aplicacion de pruena el cual contiene solo 3 vistas

- Pantalla de login
- Pantalla de home
- Pantalla de detalles
## Vistas
● Pantalla login: Esta pantalla contiene 2 inputs para iniciar sesion..
● Pantalla home: Se consume mediante una solicitud REST una lista de empleados y la muestra en pantalla
● Pantalla de detalle: Se muestran los detalles de cada empleado


## Librerias Usadas

```sh
    //Visualizar animaciones
    implementation "com.airbnb.android:lottie:3.6.1"
    //Dependencias necesarioas para las solicitudes REST
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.squareup.okhttp3:logging-interceptor:4.9.0"
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.facebook.stetho:stetho-okhttp3:1.5.1'
    //Imagen Circular
    implementation 'de.hdodenhof:circleimageview:3.1.0'
    //Base de datos
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:$room_version"
    testImplementation "androidx.room:room-testing:$room_version"
    //Visualizacion de imagenes
    implementation 'com.github.bumptech.glide:glide:4.12.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.12.0'
```

## SCREENS

![Pantalla de login](https://gitlab.com/Drabatx/apptest/-/raw/master/screens/2021-02-22_03h44_48.png)

![Pantalla de lista](https://gitlab.com/Drabatx/apptest/-/raw/master/screens/2021-02-22_03h45_04.png)

![Pantalla de detalles](https://gitlab.com/Drabatx/apptest/-/raw/master/screens/2021-02-22_03h45_14.png)
 

## TEST

![Login Test](https://gitlab.com/Drabatx/apptest/-/raw/master/screens/2021-02-22_03h06_06.png)

## Desarrollado por
Jose Luis Toxtle Ocotoxtle