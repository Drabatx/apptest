package com.drabatx.codingapptest.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.drabatx.codingapptest.model.repository.LoginRepositoryImpl

class LoginViewModel(val context: Context): ViewModel() {
    private var repository = LoginRepositoryImpl(context)

    val isLogged: LiveData<Boolean> get() = repository.isLogged
    val isLoading: LiveData<Boolean> get() = repository.isLoading
    val message: LiveData<String> get() = repository.message

    fun isLogged(){
        repository.isLogged()
    }

    fun callLogin(username: String, password: String){
        repository.login(username, password)
    }
}