package com.drabatx.codingapptest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.model.data.Employee
import com.drabatx.codingapptest.model.network.Resource
import com.drabatx.codingapptest.model.repository.EmployeeListRepositoryImpl
import com.drabatx.codingapptest.view.ui.adapter.EmployeeItemAdapter

class EmployeeListViewModel(
    val listener: EmployeeItemAdapter.OnEmployeeClickListener
) : ViewModel() {
    private val repository = EmployeeListRepositoryImpl()
    var employees: LiveData<Resource<List<Employee>?>>

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> get() = _message

    private var employeesAdapter: EmployeeItemAdapter? = null

    private val _selectEmployee = MutableLiveData<Employee>()

    val selectEmployee: LiveData<Employee> get() = _selectEmployee

    fun getEmployeeList(): LiveData<Resource<List<Employee>?>> {
        _isLoading.value = true
        employees = repository.getEmployeesList()
        return employees
    }

    fun setEmployeesInRecyclerView(list: List<Employee>?) {
        _isLoading.value = false
        if (!list.isNullOrEmpty()) {
            employeesAdapter?.setEmployeeList(list)
            employeesAdapter?.notifyDataSetChanged()
        } else {
            employeesAdapter?.setEmployeeList(arrayListOf())
            employeesAdapter?.notifyDataSetChanged()
        }
    }

    fun getRecyclerEmployeeAdapter(): EmployeeItemAdapter? {
        employeesAdapter = EmployeeItemAdapter(this, R.layout.employee_item, listener)
        return employeesAdapter
    }

    fun getEmployeeAtPosition(position: Int): Employee? {
        return employees.value?.data?.get(position)
    }

    fun selectEmployeed(position: Int): Employee? {
        return employees.value?.data?.get(position)
    }

    fun getFullNameEmployeeAtPosition(position: Int): String {
        return "${employees.value?.data?.get(position)?.firstName} ${
            employees.value?.data?.get(
                position
            )?.lastName
        }"
    }

    init {
        employeesAdapter = EmployeeItemAdapter(this, R.layout.employee_item, listener)
        employees = repository.getEmployeesList()
    }


    companion object {
        private const val TAG = "EmployeeListViewModel"
    }

}