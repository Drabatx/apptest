package com.drabatx.codingapptest.view.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.databinding.FragmentEmployeeeDetailBinding
import com.drabatx.codingapptest.model.data.Employee

class EmployeeDetailFragment : Fragment() {
    private var _binding: FragmentEmployeeeDetailBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_employeee_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val employee = arguments?.getSerializable("employee") as Employee
        Log.i(TAG, "onViewCreated: ${employee.firstName}")
        Glide.with(this).load(employee.image)
            .into(view.findViewById(R.id.ivBackgroundEmployeeDetail))
        Glide.with(this).load(employee.image).into(view.findViewById(R.id.ivEmployeeDetail))

        view.findViewById<TextView>(R.id.tvNombreEmployeeDetail)
            .setText("${employee.firstName} ${employee.lastName}")
        view.findViewById<TextView>(R.id.tvDetailsEmployeeDetail).setText("${employee.description}")

        view.findViewById<TextView>(R.id.tvRatingEmployeeDetail).setText("${employee.rating}")
        view.findViewById<RatingBar>(R.id.ratingBarEmployeeDetail).rating = employee.rating

        view.findViewById<TextView>(R.id.tvIdEmployeeDetail).setText("@${employee.id}")

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
//        view.findViewById<Button>(R.id.button_second).setOnClickListener {
//            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
//        }
    }

    companion object {
        private const val TAG = "EmployeeDetailFragment"
    }
}