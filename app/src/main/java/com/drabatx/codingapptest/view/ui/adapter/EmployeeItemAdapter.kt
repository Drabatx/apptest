package com.drabatx.codingapptest.view.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.drabatx.codingapptest.BR
import com.drabatx.codingapptest.model.data.Employee
import com.drabatx.codingapptest.viewmodel.EmployeeListViewModel

class EmployeeItemAdapter(var viewModel: EmployeeListViewModel, var resource: Int, val itemClickListener: OnEmployeeClickListener) :
    RecyclerView.Adapter<EmployeeItemAdapter.EmployeeViewHolder>() {
    var list: List<Employee>? = null
    interface OnEmployeeClickListener {
        fun onEmployeeClick(employee: Employee, position: Int)
    }
    fun setEmployeeList(list: List<Employee>) {
        this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        var layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        var binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return EmployeeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = list?.size ?: 0


    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    fun getLayoutIdForPosition(position: Int): Int {
        return resource
    }


    inner class EmployeeViewHolder(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var binding: ViewDataBinding? = null

        fun bind(employee: EmployeeListViewModel, position: Int) {
            binding?.setVariable(BR.model, employee)
            binding?.setVariable(BR.position, position)
            binding?.root?.setOnClickListener {
                itemClickListener.onEmployeeClick(employee.getEmployeeAtPosition(position)!!,position)
            }
        }

        init {
            this.binding = binding
        }
    }
}