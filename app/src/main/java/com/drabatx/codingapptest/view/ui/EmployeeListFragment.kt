package com.drabatx.codingapptest.view.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.drabatx.codingapptest.CodingApp
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.databinding.FragmentEmployeeListBinding
import com.drabatx.codingapptest.factory.EmployeeListModelFactory
import com.drabatx.codingapptest.model.data.Employee
import com.drabatx.codingapptest.view.ui.adapter.EmployeeItemAdapter
import com.drabatx.codingapptest.viewmodel.EmployeeListViewModel
import com.google.android.material.snackbar.Snackbar

class EmployeeListFragment : Fragment(),EmployeeItemAdapter.OnEmployeeClickListener{
    private var _binding: FragmentEmployeeListBinding? = null
    private val binding get() = _binding
    private val _viewModel by viewModels<EmployeeListViewModel> { EmployeeListModelFactory(this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmployeeListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.model = _viewModel

        getEmployeesList()

        binding!!.swipeEmployeeList.setOnRefreshListener {
            getEmployeesList()
        }
        _viewModel.message?.observe(viewLifecycleOwner, {
            showMessage(it)
        })
        _viewModel.isLoading?.observe(viewLifecycleOwner, {
            showLoading(it)
        })
    }

    private fun getEmployeesList() {
        _viewModel.getEmployeeList().observe(viewLifecycleOwner, { resource ->
            _viewModel.setEmployeesInRecyclerView(resource.data)
        })
    }


    override fun onEmployeeClick(employee: Employee, position: Int) {
        val bundle = bundleOf("employee" to employee)
        findNavController(this).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
    }

    fun showMessage(message: String) {
        val snack = Snackbar.make(_binding!!.rvEmployeeList, message, Snackbar.LENGTH_LONG)
        snack.setAction(R.string.accept) {
            snack.dismiss()
        }
        snack.show()
    }

    fun showLoading(isLoading: Boolean) {
        binding!!.swipeEmployeeList.isRefreshing = isLoading
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        private const val TAG = "EmployeeListFragment"
    }
}