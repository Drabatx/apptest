package com.drabatx.codingapptest.view.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.common.MainActivity
import com.drabatx.codingapptest.databinding.ActivityLoginBinding
import com.drabatx.codingapptest.factory.EmployeeListModelFactory
import com.drabatx.codingapptest.factory.LoginModelFactory
import com.drabatx.codingapptest.viewmodel.EmployeeListViewModel
import com.drabatx.codingapptest.viewmodel.LoginViewModel
import com.google.android.material.snackbar.Snackbar

class LoginActivity : AppCompatActivity() {
    private lateinit var _binding: ActivityLoginBinding
    private val _viewModel by viewModels<LoginViewModel> { LoginModelFactory(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(_binding.root)

//        _viewModel = ViewModelProvider(this).get()
        _binding.viewModel = _viewModel
        _binding.lifecycleOwner = this

        _viewModel?.isLogged()
        _viewModel?.isLogged.observe(this, {
            if (it) {
                goToMain()
            }
        })

        _viewModel.message?.observe(this, { message ->
            showMessage(message)
        })
    }


    fun showMessage(message: String) {
        val snack = Snackbar.make(_binding.buttonLogin, message, Snackbar.LENGTH_LONG)
        snack.setAction(R.string.accept) {
            snack.dismiss()
        }
        snack.show()
    }

    fun goToMain() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}