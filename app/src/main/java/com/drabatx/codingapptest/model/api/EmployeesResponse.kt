package com.drabatx.codingapptest.model.api

import com.drabatx.codingapptest.model.data.Employee

class EmployeesResponse(val employees: List<Employee>) {}