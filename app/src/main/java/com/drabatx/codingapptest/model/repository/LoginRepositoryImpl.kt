package com.drabatx.codingapptest.model.repository

import android.content.Context
import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.model.data.User
import com.drabatx.codingapptest.model.storage.UserPreferences

class LoginRepositoryImpl(val context: Context) : LoginRepository {

    private val _isLogged = MutableLiveData<Boolean>()
    val isLogged: LiveData<Boolean> get() = _isLogged

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> get() = _message

    override fun isLogged() {
        try {
            val userPreferences: UserPreferences? = UserPreferences(context)
            _isLogged.value = userPreferences?.getData(UserPreferences.IS_LOGGED).toBoolean()
        } catch (e: Exception) {
            _isLogged.value = false
        }
    }

    override fun login(username: String, password: String) {
        _isLoading.value = true
        Handler().postDelayed(Runnable {
            val user = User(username)
            val preferences = UserPreferences(context)
            preferences.saveUser(user)
            preferences.setData(UserPreferences.IS_LOGGED, "true")
            _isLoading.value = false
            _message.value = context.getString(R.string.success_login)
            _isLogged.value = true

        }, 2000)
    }
}