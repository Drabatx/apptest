package com.drabatx.codingapptest.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.drabatx.codingapptest.model.data.Employee

@Database(entities = [Employee::class], version = EmployeeDb.VERSION)
abstract class EmployeeDb : RoomDatabase() {
    abstract fun employeeDao(): EmployeeDao

    companion object{
        const val VERSION = 1
    }
}