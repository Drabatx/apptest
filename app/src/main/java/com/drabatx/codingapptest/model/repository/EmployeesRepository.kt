package com.drabatx.codingapptest.model.repository

import androidx.lifecycle.LiveData
import com.drabatx.codingapptest.model.data.Employee
import com.drabatx.codingapptest.model.network.Resource

interface EmployeesRepository {
    fun getEmployeesList(): LiveData<Resource<List<Employee>?>>
}