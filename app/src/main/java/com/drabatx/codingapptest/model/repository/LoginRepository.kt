package com.drabatx.codingapptest.model.repository

interface LoginRepository {
    fun isLogged()
    fun login(username: String, password: String)
}