package com.drabatx.codingapptest.model.storage

import android.content.Context
import android.content.SharedPreferences
import com.drabatx.codingapptest.model.data.User

class UserPreferences (context: Context?){
    private var sharedPreferences: SharedPreferences


    fun clearData() {
        sharedPreferences.edit().clear().apply()
    }

    fun saveUser(usuario: User) {
        val map = usuario.toMap()
        for (item in map){
            setData(item.key,item.value)
        }
    }

    fun getData(field: String): String? {
        return sharedPreferences.getString(field, "")
    }

    fun setData(field: String, value: String) {
        if (value.isBlank()){
            sharedPreferences.edit().putString(field, "").apply()
        }else{
            sharedPreferences.edit().putString(field, value).apply()
        }
    }

    init {
        sharedPreferences = context?.getSharedPreferences("USER_PREFERENCES", Context.MODE_PRIVATE)!!
    }

    companion object {
        const val TAG = "UserPreferences"
        const val USERNAME = "username"
        const val IS_LOGGED = "isLogged"
        const val CURRENT_SCREEN = "current_screen"
    }

}