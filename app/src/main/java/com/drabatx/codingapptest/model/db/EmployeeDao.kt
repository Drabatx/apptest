package com.drabatx.codingapptest.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.drabatx.codingapptest.model.data.Employee

@Dao
abstract class EmployeeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(employee: Employee)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun inserEmployees(employees: List<Employee>)

    @Query("SELECT * FROM employee WHERE id = :id")
    abstract fun findById(id: Int):LiveData<Employee>

    @Query("SELECT * FROM employee ORDER BY ID DESC") abstract fun getEmployees():LiveData<List<Employee>>

    @Update
    abstract fun updateEmployee(vararg employees: Employee)

    @Delete
    abstract fun delete(employee: Employee)

    @Query("DELETE FROM employee")
    abstract fun deleteAll()
}