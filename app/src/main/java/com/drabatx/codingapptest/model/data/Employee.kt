package com.drabatx.codingapptest.model.data

import androidx.room.Entity
import java.io.Serializable

@Entity(primaryKeys = ["id"])
data class Employee(
    val description: String,
    val firstName: String,
    val id: Int,
    val image: String,
    val lastName: String,
    val rating: Float
):Serializable