package com.drabatx.codingapptest.model.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.drabatx.codingapptest.CodingApp
import com.drabatx.codingapptest.R
import com.drabatx.codingapptest.db.EmployeeDao
import com.drabatx.codingapptest.model.api.ApiAdapter
import com.drabatx.codingapptest.model.api.ApiService
import com.drabatx.codingapptest.model.api.EmployeesResponse
import com.drabatx.codingapptest.model.data.Employee
import com.drabatx.codingapptest.model.network.NetworkBoundResource
import com.drabatx.codingapptest.model.network.Resource
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

data class EmployeeListResponse(val employees: List<Employee>)

class EmployeeListRepositoryImpl() : EmployeesRepository {

    private val api: ApiService

    private val employeeDao: EmployeeDao

    override fun getEmployeesList(): LiveData<Resource<List<Employee>?>> {
        return object : NetworkBoundResource<List<Employee>, EmployeesResponse>() {
            override fun saveCallResult(item: EmployeesResponse) {
                employeeDao.inserEmployees(item.employees)
            }

            override fun loadFromDb(): LiveData<List<Employee>> {
                return employeeDao.getEmployees()
            }

            override fun createCall(): Call<EmployeesResponse> {
                return api.getCoupons()
            }
        }.asLiveData
    }

    init {
        employeeDao = CodingApp.getRoom()?.employeeDao()!!
        api = ApiAdapter.createService(ApiService::class.java)
    }
}