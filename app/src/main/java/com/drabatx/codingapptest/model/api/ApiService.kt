package com.drabatx.codingapptest.model.api

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET(" ")
    fun getCoupons(): Call<EmployeesResponse>
}