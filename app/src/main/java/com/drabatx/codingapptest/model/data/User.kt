package com.drabatx.codingapptest.model.data

data class User(val username: String){
    fun toMap(): Map<String, String> {
        val map = mutableMapOf<String, String>()
        map.put("username", if(username.isEmpty())"" else  username)
        return map
    }
}
