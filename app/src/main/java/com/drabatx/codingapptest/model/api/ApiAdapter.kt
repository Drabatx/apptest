package com.drabatx.codingapptest.model.api

import com.drabatx.codingapptest.BuildConfig
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiAdapter {
    val BASE_URL = "https://demo3535907.mockable.io"
    private val client = buildClient()
    private val retrofit = buildRetrotif(client!!)

    private fun buildClient(): OkHttpClient? {
        val builder = OkHttpClient.Builder()
            .addInterceptor(Interceptor { chain ->
                var request = chain.request()
                val builder = request.newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("language", "en")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                request = builder.build()
                return@Interceptor chain.proceed(request)
            })
            .callTimeout(120, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInterceptor())
        }
        return builder.build()
    }

    private fun buildRetrotif(client: OkHttpClient): Retrofit? {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    public fun <T> createService(service: Class<T>?): T {
        return retrofit!!.create(service)
    }
}