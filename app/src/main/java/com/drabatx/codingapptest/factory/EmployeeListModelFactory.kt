package com.drabatx.codingapptest.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.drabatx.codingapptest.view.ui.adapter.EmployeeItemAdapter
import com.drabatx.codingapptest.viewmodel.EmployeeListViewModel

class EmployeeListModelFactory(val listener: EmployeeItemAdapter.OnEmployeeClickListener) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EmployeeListViewModel(listener) as T
    }
}