package com.drabatx.codingapptest.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.drabatx.codingapptest.view.ui.adapter.EmployeeItemAdapter
import com.drabatx.codingapptest.viewmodel.EmployeeListViewModel
import com.drabatx.codingapptest.viewmodel.LoginViewModel

class LoginModelFactory (val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(context) as T
    }
}