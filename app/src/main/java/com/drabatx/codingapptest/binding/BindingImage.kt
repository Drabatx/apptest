package com.drabatx.codingapptest.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.drabatx.codingapptest.CodingApp
import com.drabatx.codingapptest.R
import de.hdodenhof.circleimageview.CircleImageView

@BindingAdapter("url")
fun loadUrl(iv: CircleImageView, url: String?) {
    if (url == null) {
        iv.setImageDrawable(CodingApp.getContext().getDrawable(R.drawable.ic_launcher_background))
    } else {
        Glide.with(iv).load(url).into(iv)
    }
}

//@BindingAdapter("url")
//fun CircleImageView.loadUrl(view: ImageView, url: String, error: Drawable) {
////    Picasso.get().load(url).error(error).into(view)
//    Glide.with(this).load(url).error(error).into(this)
//
//}
