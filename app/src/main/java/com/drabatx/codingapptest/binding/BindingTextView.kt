package com.drabatx.codingapptest.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.drabatx.codingapptest.utils.Utils

@BindingAdapter("textFloat")
fun TextView.setFloat(float: Float) {
    text = String.format("%.1f", float)
}