package com.drabatx.codingapptest.binding

import androidx.databinding.BindingAdapter
import com.drabatx.codingapptest.CodingApp
import com.drabatx.codingapptest.R
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("passwordValidator")
fun inputPassword(input: TextInputLayout, password: String) {
    if (password.isEmpty()) {
        input.error = CodingApp.getContext().getString(R.string.error_password)
    } else {
        input.error = null
        return
    }
}

@BindingAdapter("usernameValidator")
fun inputUsername(input: TextInputLayout, username: String) {
    if (username.isEmpty()) {
        input.error = CodingApp.getContext().getString(R.string.error_username)
    } else {
        input.error = null
        return
    }
}
