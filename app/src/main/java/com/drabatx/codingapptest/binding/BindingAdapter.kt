package com.drabatx.codingapptest.binding

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("visible")
fun View.bindVisible(visible: Boolean?) {
    visibility = if (visible == true) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("gone")
fun View.bindGone(visible: Boolean?) {
    visibility = if (visible == true) View.VISIBLE else View.GONE
}

