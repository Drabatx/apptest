package com.drabatx.codingapptest.utils

import java.text.DecimalFormat

class Utils {
    companion object{
        fun numberFormat(number: Float, decimals: Int): String? {
            val format = "0"
            for (i in 1..decimals) format.plus("0")
            val dec = DecimalFormat("#,${format}")
            val credits = dec.format(number)
            return credits
        }
    }
}