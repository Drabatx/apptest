package com.drabatx.codingapptest

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.drabatx.codingapptest.db.EmployeeDb
import com.facebook.stetho.Stetho
import com.drabatx.codingapptest.BuildConfig
class CodingApp : Application() {
    override fun onCreate() {
        super.onCreate()
        context = this.applicationContext
        mApplication = this
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
        room = Room.databaseBuilder(this, EmployeeDb::class.java, "employees").build()
    }

    companion object {
        private var context: Context? = null
        private var room: EmployeeDb? = null
        private var mApplication:Application?=null
        fun getContext() = context!!
        fun getInstance():Application = mApplication!!
        fun getRoom() = room
    }
}