package com.drabatx.codingapptest

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.drabatx.codingapptest.viewmodel.LoginViewModel
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1], application = CodingApp::class)
class LoginViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    val loginViewModel = LoginViewModel(ApplicationProvider.getApplicationContext())

    @Test
    fun test_logged() {
        val observer = object : Observer<Any> {
            override fun onChanged(isLogged: Any) {
                if (isLogged == true) {
                    print("Has iniciado Sesion")
                } else {
                    print("No has iniciado sesion")
                }
            }
        }

        loginViewModel.isLogged()
        loginViewModel.isLogged?.observeForever(observer)


    }

    @Test
    fun test_login() {
        val observer = object : Observer<Any> {
            override fun onChanged(message: Any) {
                print(message)
            }
        }

        loginViewModel.message?.observeForever(observer)
//        loginViewModel.isLogged?.observeForever(observerLogged)

        loginViewModel.callLogin("ejemplo", "ejemplo")


    }
}